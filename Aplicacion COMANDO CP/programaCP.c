#include <stdio.h>
#include <stdlib.h>




int main(int argc, char *argv[]){
	
	if(argc !=3){
		printf("No se especificaron los nombres de los archivos...");
		return 0;
	}

	FILE * read;
	FILE * write;
	
	read = fopen(argv[1], "r" );
	write = fopen(argv[2], "w" );
	
	if(read ==NULL){
		printf("NO se pudo abrir el archivo....");
		return 0;
	}
	
	char caracter = ' ';
	
	fscanf(read, "%c",&caracter);
	
	while(!feof(read)){
		
		fprintf(write, "%c", caracter);
		fscanf(read, "%c",&caracter);
	}
	 fclose(read);
	 fclose(write);
	 
	 return 0;

}
